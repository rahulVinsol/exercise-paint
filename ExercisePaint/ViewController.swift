//
//  ViewController.swift
//  ExercisePaint
//
//  Created by Rahul Rawat on 29/06/21.
//

import UIKit
import PencilKit

class ViewController: UIViewController {
    
    @IBOutlet weak var pencilCanvasView: PKCanvasView!
    
    private weak var toolPicker: PKToolPicker?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPencilKitView()
    }

    private func setPencilKitView() {
        pencilCanvasView.delegate = self
        pencilCanvasView.drawing = PKDrawing()
        pencilCanvasView.alwaysBounceVertical = false
        pencilCanvasView.backgroundColor = .clear
        pencilCanvasView.isOpaque = false
        
        pencilCanvasView.allowsFingerDrawing = true
    }
        
    override func viewDidAppear(_ animated: Bool) {
        if let window = view.window, let toolPicker = PKToolPicker.shared(for: window) {
            self.toolPicker = toolPicker
            toolPicker.addObserver(pencilCanvasView)
            pencilCanvasView.becomeFirstResponder()
            
            toolPicker.setVisible(true, forFirstResponder: pencilCanvasView)
        }
    }
}

extension ViewController: PKCanvasViewDelegate {
    
}
